<?php
/*
Plugin Name: ArtSpeak Job Postings
Plugin URI: http://artspeakcreative.com
Description: Job postings for website
Version: 0.0.1
Author: Drew Sartorius
Author URI: http://artspeakcreative.com
License: GPL2
 */

//  PLUGIN FUNCTIONS
function ak_jobs_activation()
{
 // Run when plugin is activated
}
register_activation_hook(__FILE__, 'ak_jobs_activation');

function ak_jobs_deactivation()
{
 // Run when plugin is deactivated
}
register_deactivation_hook(__FILE__, 'ak_jobs_deactivation');

function ak_jobs_uninstall()
{
 // Run when plugin is uninstalled
}
register_uninstall_hook(__FILE__, 'ak_jobs_uninstall');

// HOOKS FUNCTIONS
global $AK_JOBS_ROOT;
$AK_JOBS_ROOT = WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__));
include_once "functions/index.php";

// Add Fields
include_once "fields/option_fields.php";

// Register Scripts
function ak_jobs_enqueue_scripts()
{
 wp_enqueue_style('ak_jobs-css', plugins_url('/assets/css/jobs.min.css', __FILE__), array(), '', 'all');
 //  wp_register_script('script_name', $path, array('jquery'), '', true);
 //  wp_enqueue_script('script_name');
}
add_action('wp_enqueue_scripts', 'ak_jobs_enqueue_scripts');
