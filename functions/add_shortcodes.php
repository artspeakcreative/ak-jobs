<?php

function ak_job_shortcodes()
{
 global $AK_JOBS_ROOT;
 $plugin_components = $AK_JOBS_ROOT . '/templates/views/components/';
 Component::load_components($plugin_components);
}
add_action('ak_run_actions', 'ak_job_shortcodes');
