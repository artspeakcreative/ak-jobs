<?php

add_filter('template_include', 'ak_job_override_templates');
function ak_job_override_templates($template)
{
 global $AK_JOBS_ROOT;

 if (is_post_type_archive('job')) {
  $theme_files = array('archive-job.php', 'ak-job/archive-job.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_JOBS_ROOT . '/templates/archive-job.php';
  }
 } elseif (is_singular('job')) {
  $theme_files = array('single-job.php', 'ak-job/single-job.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_JOBS_ROOT . '/templates/single-job.php';
  }
 } elseif (is_tax('job_team')) {
  $theme_files = array('taxonomy-job_team.php', 'ak-jobs/taxonomy-job_team.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_JOBS_ROOT . '/templates/taxonomy-job_team.php';
  }
 } elseif (is_tax('job_type')) {
  $theme_files = array('taxonomy-job_type.php', 'ak-jobs/taxonomy-job_type.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_JOBS_ROOT . '/templates/taxonomy-job_type.php';
  }
 }
 return $template;
}
