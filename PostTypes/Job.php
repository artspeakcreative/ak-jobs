<?php
namespace PostType;

/*
Job Post Type
 */

class Job extends BasePost
{
 const POST_TYPE = "Job";

 /**- - - - - - - - - - - - - -
  * CONSTRUCTOR
  * - - - - - - - - - - - - - - */
 public function __construct($post)
 {
  parent::__construct($post);

  $this->team = $this->getTeam();
  $this->type = $this->getType();
 }

 /**- - - - - - - - - - - - - -
  * STATIC FUNCTIONS
  * - - - - - - - - - - - - - - */

 /**
  * Initial setup of all static functions
  *
  * @return void
  */
 public static function setup()
 {
  Job::register();
  Job::fields();
  Job::hooks();
 }

 /**
  * Register the class in WP
  *
  * @return void
  */
 public static function register()
 {
  register_extended_post_type(self::POST_TYPE, array(
   'show_in_feed' => true,
   'menu_icon' => 'dashicons-money-alt',
   'enter_title_here' => 'Job Title',
   'admin_cols' => array(
    'featured_image' => array(
     'title' => 'Image',
     'featured_image' => 'tiny',
    ),
    'title',
    // A taxonomy terms column:
    'job_team' => array(
     'taxonomy' => 'job_team',
    ),
    'job_type' => array(
     'taxonomy' => 'job_type',
    ),
   ),
   'admin_filters' => array(
    'job_team' => array(
     'title' => 'Job Team',
     'taxonomy' => 'job_team',
    ),
    'job_type' => array(
     'title' => 'Job Type',
     'taxonomy' => 'job_type',
    ),
   ),
  ), array(
   # Override the base names used for labels:
   'singular' => 'Job',
   'plural' => 'Jobs',
   'slug' => 'jobs',
  ));

  register_extended_taxonomy(
   'job_team',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Job Team',
    'plural' => 'Job Teams',
    'slug' => 'job_teams',
   ));

  register_extended_taxonomy(
   'job_type',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Job Type',
    'plural' => 'Job Types',
    'slug' => 'job_types',
   ));
 }

 /**
  * Add all the ACF fields to the post type
  *
  * @return void
  */
 public static function fields()
 {
  $group_key = 'group_' . str_replace(' ', '_', self::POST_TYPE) . '_post_type';
  $group = array(
   'key' => $group_key,
   'title' => self::POST_TYPE . " Settings",
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'post_type',
      'operator' => '==',
      'value' => strtolower(self::POST_TYPE),
     ),
    ),
   ),

   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  acf_add_local_field(array(
   'key' => $group_key . "_report_to",
   'label' => 'Reports To',
   'name' => 'job_report_to',
   'type' => 'text',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_starting_pay",
   'label' => 'Starting Pay',
   'name' => 'starting_pay',
   'type' => 'text',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_position_type",
   'label' => 'Position Type Description',
   'name' => 'position_type_description',
   'type' => 'textarea',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_position_snppet",
   'label' => 'Position Snippet',
   'name' => 'position_snippet',
   'type' => 'textarea',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_job_description",
   'label' => 'Job Description',
   'name' => 'job_description',
   'type' => 'wysiwyg',
   'parent' => $group_key,
  ));
 }

 public static function hooks()
 {
  // add_action('pre_get_posts', 'PostType\Job::ak_sort_jobs_by_date', 1);
 }

 /**- - - - - - - - - - - - - -
  * HELPER FUNCTIONS
  * - - - - - - - - - - - - - - */
 public function getTeam()
 {
  if ($this->team) {return $this->team;}
  $teams = wp_get_post_terms($this->ID, 'job_team');
  return array_shift($teams);
 }

 public function getType()
 {
  if ($this->type) {return $this->type;}
  $types = wp_get_post_terms($this->ID, 'job_type');
  return array_shift($types);
 }
}

Job::setup();
