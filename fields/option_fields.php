<?php
/* ---------------------
 * ACF Settings Function
 * --------------------- */
function ak_job_add_options()
{
 if (function_exists('acf_add_local_field_group')) {
  $group_key = 'options_ak_job';
  $group = array(
   'key' => $group_key,
   'title' => "Job Posting Settings",
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'options_page',
      'operator' => '==',
      'value' => 'acf-options',
     ),
    ),
   ),
   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  acf_add_local_field(array(
   'key' => $group_key . "_core_values",
   'label' => 'Core Values',
   'name' => 'job_core_values',
   'type' => 'repeater',
   'sub_fields' => array(
    array(
     'key' => $group_key . "_core_value_name",
     'label' => 'Name',
     'name' => 'name',
     'type' => 'text',
    ),
    array(
     'key' => $group_key . "_core_value_description",
     'label' => 'Description',
     'name' => 'description',
     'type' => 'textarea',
    ),
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_company_info_link",
   'label' => 'Company Info Link',
   'name' => 'company_info_link',
   'type' => 'text',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_company_description",
   'label' => 'Company Description',
   'name' => 'company_description',
   'type' => 'textarea',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_company_video",
   'label' => 'Company Video',
   'name' => 'company_video',
   'type' => 'oembed',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_company_image",
   'label' => 'Company Image',
   'name' => 'company_image',
   'instructions' => 'Image placeholder if there is no video',
   'type' => 'image',
   'return_value' => 'id',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_job_form_id",
   'label' => 'Apply Form ID',
   'name' => 'apply_form_id',
   'type' => 'text',
   'parent' => $group_key,
  ));
 }
}
add_filter('init', 'ak_job_add_options');
