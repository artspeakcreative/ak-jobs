<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array('archive.twig', 'index.twig');

$context = Timber::get_context();

$context['title'] = 'Jobs';

$context['posts'] = Timber::get_posts();
$context['pagination'] = Timber::get_pagination();

$tag_args = array(
 'taxonomy' => 'job_team',
);
$tags = Timber::get_terms($tag_args);
$context['teamTags'] = $tags;

$tag_args = array(
 'taxonomy' => 'job_type',
);
$tags = Timber::get_terms($tag_args);
$context['typeTags'] = $tags;

Timber::render('archive-job.twig', $context);
